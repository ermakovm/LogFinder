package info.mermakov.dev.logfinder.search;

import info.mermakov.dev.logfinder.util.RandomFileReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class Search {
    private static List<Charset> supportedEncodings = Arrays.asList(StandardCharsets.UTF_8, Charset.forName("Windows-1251"));

    public static FilePages checkFile(Path file, String pattern) throws IOException {
        FilePages[] results = new FilePages[supportedEncodings.size()];
        for (int i = 0; i < supportedEncodings.size(); i++) {
            log.debug("Check file: " + file.getFileName().toString() + ", with encoding: " + supportedEncodings.get(i).toString());
            results[i] = new FilePages(file, supportedEncodings.get(i));
            RandomFileReader fr = new RandomFileReader(file);
            String line;
            long lineNum = 1;
            int pageNum = 0;
            int maxLine = SearchOptions.PAGE_SIZE;
            long pos = 0;
            Page page = new Page(lineNum, pos);
            results[i].addPage(page);
            while ((line = fr.readLine(results[i].getFileCharset())) != null) {
                if (maxLine == 0) {
                    pageNum++;
                    results[i].addPage(new Page(lineNum, pos));
                    maxLine = SearchOptions.PAGE_SIZE;
                }
                if (line.contains(pattern)) {
                    results[i].addEntry(lineNum, pageNum);
                }
                pos = fr.getOffset();
                lineNum++;
                maxLine--;
            }
        }
        long maxEntry = 0;
        int index = 0;
        for (int i = 0; i < results.length; i++) {
            if (results[i].getEntryCount() > maxEntry) {
                maxEntry = results[i].getEntryCount();
                index = i;
            }
        }
        if (!results[index].getEntriesPages().isEmpty()) {
            return results[index];
        }
        return null;
    }
}
