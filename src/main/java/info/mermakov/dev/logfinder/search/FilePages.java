package info.mermakov.dev.logfinder.search;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
public class FilePages {
    private Path file;
    private List<Page> pages;
    private List<Integer> entriesPages;
    private Charset fileCharset;

    public FilePages(Path file, Charset fileCharset) {
        this.file = file;
        this.fileCharset = fileCharset;
        this.pages = new ArrayList<>();
        this.entriesPages = new ArrayList<>();
    }

    public void addPage(Page page) {
        pages.add(page);
    }

    public void addEntry(long line, int page) {
        if (!entriesPages.contains(page)) {
            entriesPages.add(page);
        }
        pages.get(page).addEntry(line);
    }

    public long getEntryCount() {
        long result = 0;
        for (int i = 0; i < entriesPages.size(); i++) {
            int pageIndex = entriesPages.get(i);
            result += pages.get(pageIndex).getEntries().size();
        }
        return result;
    }

    public int getPageCount() {
        return pages.size();
    }

    private boolean isRoot() {
        for (File root : File.listRoots()) {
            if (root.getPath().equals(file.toString())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        if (isRoot()) {
            return file.toString();
        } else {
            return file.getFileName().toString();
        }
    }
}
