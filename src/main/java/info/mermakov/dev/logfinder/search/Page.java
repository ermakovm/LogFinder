package info.mermakov.dev.logfinder.search;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
public class Page {
    private long line;
    private long position;
    private List<Long> entries;

    public Page(long line, long position) {
        log.debug("Create page");
        this.line = line;
        this.position = position;
        this.entries = new ArrayList<>();
    }

    public void addEntry(long line) {
        log.debug("Add new entry");
        entries.add(line);
    }
}
