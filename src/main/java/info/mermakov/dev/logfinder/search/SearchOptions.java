package info.mermakov.dev.logfinder.search;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.nio.file.Path;

@AllArgsConstructor
@Getter
public class SearchOptions {
    public static final Integer PAGE_SIZE = 65536;
    private Path rootFolder;
    private String extension;
    private String searchPattern;
}
