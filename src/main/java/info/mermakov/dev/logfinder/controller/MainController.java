package info.mermakov.dev.logfinder.controller;

import info.mermakov.dev.logfinder.search.FilePages;
import info.mermakov.dev.logfinder.search.Page;
import info.mermakov.dev.logfinder.search.SearchOptions;
import info.mermakov.dev.logfinder.util.PairKeyFactory;
import info.mermakov.dev.logfinder.util.PairValueFactory;
import info.mermakov.dev.logfinder.util.RandomFileReader;
import info.mermakov.dev.logfinder.util.TreeBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class MainController implements Initializable {
    @FXML
    public TextField directoryTextBox;

    @FXML
    public TextField extensionTextBox;

    @FXML
    public TextField searchTextBox;

    @FXML
    public Button directoryButton;

    @FXML
    public Button searchButton;

    @FXML
    public TreeView<FilePages> treeView;

    @FXML
    public TabPane tabPane;

    @FXML
    public ProgressIndicator searchIndicator;

    private Map<Tab, FilePages> tabs = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        treeView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                if (treeView.getRoot() != null) {
                    if (treeView.getSelectionModel().getSelectedItem() != null) {
                        FilePages selected = treeView.getSelectionModel().getSelectedItem().getValue();
                        if (!Files.isDirectory(selected.getFile())) {
                            Tab newTab = newTab(selected);
                            tabs.put(newTab, selected);
                            tabPane.getSelectionModel().select(newTab);
                        }
                    }
                }
            }
        });
    }

    private Stage getCurrentStage(ActionEvent actionEvent) {
        Node source = (Node) actionEvent.getSource();
        return (Stage) source.getScene().getWindow();
    }

    private TableView<Pair<Long, String>> createTable(FilePages filePages, Pagination pagination) {
        log.debug("Create table");
        TableView<Pair<Long, String>> table = new TableView<>();
        TableColumn<Pair<Long, String>, Long> lineNum = new TableColumn<>("Number");
        lineNum.setSortable(false);
        TableColumn<Pair<Long, String>, String> text = new TableColumn<>("Line");
        text.setSortable(false);
        table.getColumns().add(lineNum);
        table.getColumns().add(text);
        lineNum.setCellValueFactory(new PairKeyFactory());
        text.setCellValueFactory(new PairValueFactory());

        pagination.setPageCount(filePages.getPageCount());
        pagination.setMaxPageIndicatorCount(7);
        pagination.setPageFactory(new Callback<>() {
            @Override
            public Node call(Integer parameter) {
                lineNum.setCellFactory(cell -> new TableCell<>() {
                    @Override
                    protected void updateItem(Long item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setText(null);
                            setStyle("");
                        } else {
                            setText(getItem().toString());
                            TableRow<Pair<Long, String>> row = getTableRow();
                            if (filePages.getPages().get(parameter).getEntries().contains(item)) {
                                if (row != null) {
                                    row.setStyle("-fx-background-color:pink");
                                }
                            } else {
                                if (row != null) {
                                    row.setStyle("");
                                }
                            }
                        }
                    }
                });
                List<Pair<Long, String>> list = new ArrayList<>();
                try {
                    RandomFileReader randomFileReader = new RandomFileReader(filePages.getFile());
                    Page toRead = filePages.getPages().get(parameter);
                    randomFileReader.seek(toRead.getPosition());
                    String line;
                    long i = toRead.getLine();
                    int maxCount = SearchOptions.PAGE_SIZE;
                    while ((line = randomFileReader.readLine(filePages.getFileCharset())) != null) {
                        if (maxCount == 0) {
                            break;
                        }
                        list.add(new Pair<>(i, line));
                        i++;
                        maxCount--;
                    }
                    randomFileReader.close();
                } catch (IOException exception) {
                    log.error(exception.getMessage(), exception);
                    showAlert(exception.getMessage());
                }
                ObservableList<Pair<Long, String>> result = FXCollections.observableArrayList(list);
                table.setItems(result);
                return table;
            }
        });
        return table;
    }

    private Tab newTab(FilePages filePages) {
        log.debug("Create tab");
        AtomicInteger page = new AtomicInteger(-1);
        AtomicLong row = new AtomicLong(-1);
        Path source = filePages.getFile();
        Tab tab = new Tab();

        Pagination pagination = new Pagination();
        TableView<Pair<Long, String>> table = createTable(filePages, pagination);

        VBox vbox = new VBox();
        HBox hbox = new HBox();

        Button btnFirst = new Button();
        btnFirst.setText("First Entry");
        btnFirst.setOnAction(event -> {
            log.debug("Button 'First Entry' clicked");
            int firstEntryPage = filePages.getEntriesPages().get(0);
            long firstEntryRow = filePages.getPages().get(firstEntryPage).getEntries().get(0);
            page.set(firstEntryPage);
            row.set(firstEntryRow);
            if (pagination.getCurrentPageIndex() != firstEntryPage) {
                pagination.setCurrentPageIndex(firstEntryPage);
            }
            setFocus(table, (int) firstEntryRow - firstEntryPage * SearchOptions.PAGE_SIZE - 1);
        });

        Button btnPrevEntry = new Button();
        btnPrevEntry.setText("<< Entry");
        btnPrevEntry.setOnAction(e -> {
            log.debug("Button '<< Entry' clicked");
            getItemToSelect(filePages, false, page, row);
            if (pagination.getCurrentPageIndex() != page.get()) {
                pagination.setCurrentPageIndex(page.get());
            }
            setFocus(table, (int) (row.get() - (page.get() * SearchOptions.PAGE_SIZE)) - 1);
        });


        Button btnNextEntry = new Button();
        btnNextEntry.setText("Entry >>");
        btnNextEntry.setOnAction(e -> {
            log.debug("Button 'Entry >>' clicked");
            getItemToSelect(filePages, true, page, row);
            if (pagination.getCurrentPageIndex() != page.get()) {
                pagination.setCurrentPageIndex(page.get());
            }
            setFocus(table, (int) (row.get() - (page.get() * SearchOptions.PAGE_SIZE)) - 1);
        });

        Button btnLast = new Button();
        btnLast.setText("Last Entry");
        btnLast.setOnAction(event -> {
            log.debug("Button 'Last Entry' clicked");
            int lastEntryPage = filePages.getEntriesPages().get(filePages.getEntriesPages().size() - 1);
            long lastEntryRow = filePages.getPages().get(lastEntryPage).getEntries().get(filePages.getPages().get(lastEntryPage).getEntries().size() - 1);
            page.set(lastEntryPage);
            row.set(lastEntryRow);
            pagination.setCurrentPageIndex(lastEntryPage);
            setFocus(table, (int) (lastEntryRow - lastEntryPage * SearchOptions.PAGE_SIZE - 1));
        });

        hbox.getChildren().addAll(btnFirst, btnPrevEntry, btnNextEntry, btnLast);
        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(25);

        vbox.getChildren().addAll(pagination, hbox);
        VBox.setVgrow(pagination, Priority.ALWAYS);

        tab.setContent(vbox);
        tab.setText(source.getFileName().toString());
        tab.setOnClosed(e -> {
            if (tabPane.getTabs().isEmpty()) {
                tabs.remove(tab);
            }
        });

        tabPane.getTabs().add(tab);
        return tab;
    }

    private void setFocus(TableView<Pair<Long, String>> table, int rowIndex) {
        table.requestFocus();
        table.getSelectionModel().select(rowIndex);
        table.getFocusModel().focus(rowIndex);
        table.scrollTo(rowIndex);
    }

    private void getItemToSelect(FilePages filePages, boolean getNext, AtomicInteger lastPage, AtomicLong lastRow) {
        int firstEntryPage = filePages.getEntriesPages().get(0);
        long firstEntryRow = filePages.getPages().get(firstEntryPage).getEntries().get(0);
        if (lastPage.get() == -1 || lastRow.get() == -1) {
            lastPage.set(firstEntryPage);
            lastRow.set(firstEntryRow);
            return;
        }
        if (getNext) {
            int lastEntryPage = filePages.getEntriesPages().get(filePages.getEntriesPages().size() - 1);
            long lastEntryRow = filePages.getPages().get(lastEntryPage).getEntries().get(filePages.getPages().get(lastEntryPage).getEntries().size() - 1);
            if (lastPage.get() == lastEntryPage && lastRow.get() == lastEntryRow) {
                return;
            }
            int pageIndex = filePages.getEntriesPages().indexOf(lastPage.get());
            for (int i = pageIndex; i < filePages.getEntriesPages().size(); i++) {
                for (int j = 0; j < filePages.getPages().get(filePages.getEntriesPages().get(i)).getEntries().size(); j++) {
                    long row = filePages.getPages().get(filePages.getEntriesPages().get(i)).getEntries().get(j);
                    if (row > lastRow.get()) {
                        lastPage.set(filePages.getEntriesPages().get(i));
                        lastRow.set(row);
                        return;
                    }
                }
            }
        } else {
            if (lastPage.get() == firstEntryPage && lastRow.get() == firstEntryRow) {
                return;
            }
            int pageIndex = filePages.getEntriesPages().indexOf(lastPage.get());
            for (int i = pageIndex; i >= 0; i--) {
                for (int j = filePages.getPages().get(filePages.getEntriesPages().get(i)).getEntries().size() - 1; j >= 0; j--) {
                    long row = filePages.getPages().get(filePages.getEntriesPages().get(i)).getEntries().get(j);
                    if (row < lastRow.get()) {
                        lastPage.set(filePages.getEntriesPages().get(i));
                        lastRow.set(row);
                        return;
                    }
                }
            }
        }
    }


    public void openFileChooserClicked(ActionEvent actionEvent) {
        log.debug("Directory selection dialog");
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(getCurrentStage(actionEvent));
        if (selectedDirectory != null) {
            directoryTextBox.setText(selectedDirectory.getAbsolutePath());
        }
    }

    public void onSearchClicked(ActionEvent actionEvent) {
        log.debug("Search button clicked");
        String directory = directoryTextBox.getText();
        String extension = extensionTextBox.getText();
        String searchText = searchTextBox.getText();
        if (directory.isEmpty()) {
            showAlert("Please select a directory");
            return;
        }
        if (extension.isEmpty()) {
            showAlert("Please enter extension");
            return;
        }
        if (searchText.isEmpty()) {
            showAlert("Please enter text");
            return;
        }
        Path pathToDirectory = Paths.get(directory);
        TreeBuilder task = new TreeBuilder(new SearchOptions(pathToDirectory, extension, searchText));
        task.setOnSucceeded(event -> {
            TreeItem<FilePages> root = task.getValue();
            if (root.getChildren().isEmpty()) {
                log.info("Nothing found");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                alert.setContentText("Nothing found");
                alert.showAndWait();
            } else {
                treeView.setRoot(root);
            }
            searchIndicator.visibleProperty().setValue(false);
        });
        task.setOnFailed(event -> {
            task.getException().printStackTrace();
            showAlert(task.getMessage());
        });
        searchIndicator.visibleProperty().setValue(true);
        (new Thread(task)).start();
    }

    private void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
