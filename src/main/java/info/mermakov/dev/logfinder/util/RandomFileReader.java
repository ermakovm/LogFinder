package info.mermakov.dev.logfinder.util;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.Path;

public class RandomFileReader {
    private final int BUFFER_SIZE = 65536;
    private byte[] buffer;
    private int bufferEnd = 0;
    private int bufferPos = 0;
    private long realPos;

    private RandomAccessFile randomFile;

    public RandomFileReader(Path fileInput) throws IOException {
        randomFile = new RandomAccessFile(fileInput.toFile(), "r");
        realPos = randomFile.getFilePointer();
        this.buffer = new byte[BUFFER_SIZE];
    }

    public long getOffset() {
        return (realPos - bufferEnd) + bufferPos;
    }

    public String readLine(Charset charset) throws IOException {
        String str;
        // Fill the buffer
        if (bufferEnd - bufferPos <= 0) {
            if (fillBuffer() < 0) {
                return null;
            }
        }
        // Find line terminator from buffer
        int lineEnd = -1;
        for (int i = bufferPos; i < bufferEnd; i++) {
            if (buffer[i] == '\n') {
                lineEnd = i;
                break;
            }
        }
        // Line terminator not found from buffer
        if (lineEnd < 0) {
            StringBuilder sb = new StringBuilder(256);

            int c;
            while (((c = read()) != -1) && (c != '\n')) {
                if ((char) c != '\r') {
                    sb.append((char) c);
                }
            }
            if ((c == -1) && (sb.length() == 0)) {
                return null;
            }
            return sb.toString();
        }
        if (lineEnd > 0 && buffer[lineEnd - 1] == '\r') {
            str = new String(buffer, bufferPos, lineEnd - bufferPos - 1,
                    charset);
        } else {
            str = new String(buffer, bufferPos, lineEnd - bufferPos, charset);
        }
        bufferPos = lineEnd + 1;
        return str;
    }

    private int read() throws IOException {
        if (bufferPos >= bufferEnd) {
            if (fillBuffer() < 0) {
                return -1;
            }
        }
        if (bufferEnd == 0) {
            return -1;
        } else {
            return buffer[bufferPos++];
        }
    }

    public void seek(long pos) throws IOException {
        int n = (int) (realPos - pos);
        if (n >= 0 && n <= bufferEnd) {
            bufferPos = bufferEnd - n;
        } else {
            randomFile.seek(pos);
            bufferEnd = 0;
            bufferPos = 0;
            realPos = randomFile.getFilePointer();
        }
    }

    private int fillBuffer() throws IOException {
        int n = randomFile.read(buffer, 0, BUFFER_SIZE);
        if (n >= 0) {
            realPos += n;
            bufferEnd = n;
            bufferPos = 0;
        }
        return n;
    }

    public void close() throws IOException {
        randomFile.close();
    }
}
