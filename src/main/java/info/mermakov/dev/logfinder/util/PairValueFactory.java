package info.mermakov.dev.logfinder.util;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import javafx.util.Pair;

public class PairValueFactory implements Callback<TableColumn.CellDataFeatures<Pair<Long, String>, String>, ObservableValue<String>> {
    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<Pair<Long, String>, String> data) {
        return new ReadOnlyObjectWrapper<>(data.getValue().getValue());
    }
}
