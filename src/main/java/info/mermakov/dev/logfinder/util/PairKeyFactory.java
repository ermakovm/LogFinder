package info.mermakov.dev.logfinder.util;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import javafx.util.Pair;


public class PairKeyFactory implements Callback<TableColumn.CellDataFeatures<Pair<Long, String>, Long>, ObservableValue<Long>> {
    @Override
    public ObservableValue<Long> call(TableColumn.CellDataFeatures<Pair<Long, String>, Long> data) {
        return new ReadOnlyObjectWrapper<>(data.getValue().getKey());
    }
}
