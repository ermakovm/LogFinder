package info.mermakov.dev.logfinder.util;

import info.mermakov.dev.logfinder.search.FilePages;
import info.mermakov.dev.logfinder.search.Search;
import info.mermakov.dev.logfinder.search.SearchOptions;
import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;

@Slf4j
public class TreeBuilder extends Task<TreeItem<FilePages>> {
    private DirectoryStream.Filter<Path> filter = entry -> {
        String name = entry.toString();
        return !name.toUpperCase().contains("$RECYCLE.BIN")
                && !name.contains("System Volume Information");
    };
    private SearchOptions options;
    private PathMatcher pathMatcher;

    public TreeBuilder(SearchOptions options) {
        //https://stackoverflow.com/questions/1384947/java-find-txt-files-in-specified-folder
        this.options = options;
        pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**." + options.getExtension());
    }

    private void build(Path root, TreeItem<FilePages> tree) throws IOException {
        log.debug("Get items in directory: " + root.getFileName().toString());
        DirectoryStream<Path> stream = Files.newDirectoryStream(root, filter);
        for (Path entry : stream) {
            if (Files.isDirectory(entry)) {
                log.debug("Check directory: " + entry.getFileName().toString());
                TreeItem<FilePages> subRoot = getFolder(entry);
                build(entry, subRoot);
                if (!subRoot.getChildren().isEmpty()) {
                    tree.getChildren().add(subRoot);
                }
            } else {
                if (pathMatcher.matches(entry)) {
                    log.debug("Check file: " + entry.getFileName().toString());
                    FilePages filePages = Search.checkFile(entry, options.getSearchPattern());
                    if (filePages != null) {
                        tree.getChildren().add(new TreeItem<>(filePages));
                    }
                }
            }
        }
        stream.close();
    }

    private TreeItem<FilePages> getFolder(Path name) {
        return new TreeItem<>(new FilePages(name, StandardCharsets.UTF_8));
    }

    @Override
    protected TreeItem<FilePages> call() throws IOException {
        Path rootFolder = options.getRootFolder();
        TreeItem<FilePages> root = getFolder(rootFolder);
        log.debug("Start finding in root folder");
        build(rootFolder, root);
        log.debug("Tree created");
        return root;
    }

}
