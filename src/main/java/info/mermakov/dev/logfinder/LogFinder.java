package info.mermakov.dev.logfinder;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogFinder extends Application {
    public static void main(String[] args) {
        log.debug("Start application");
        launch(args);
        log.debug("Stop application");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.debug("Load resource");
        Parent parent = FXMLLoader.load(getClass().getResource("/mainForm.fxml"));
        primaryStage.setTitle("LogFinder");
        log.debug("Set Scene");
        primaryStage.setScene(new Scene(parent, 1024, 768));
        primaryStage.setMinHeight(768);
        primaryStage.setMinWidth(1024);
        log.debug("Show Stage");
        primaryStage.show();
    }
}
